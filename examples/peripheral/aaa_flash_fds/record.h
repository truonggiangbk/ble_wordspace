#ifndef RECORD_H__
#define RECORD_H__
#include <stdint.h>
#include <stdbool.h>
#include "fds.h"
#include "app_error.h"

void record_init(void);
void record_gc(void);
ret_code_t record_delete(uint32_t fid, uint32_t key);
ret_code_t record_find(uint32_t fid, uint32_t key);
ret_code_t record_write(uint32_t fid, uint32_t key, void const * p_data, uint32_t len);
ret_code_t record_update(uint32_t fid, uint32_t key, void const * p_data, uint32_t len);
ret_code_t record_read(uint32_t fid, uint32_t key, void const * p_data, uint32_t* len);
#endif //RECORD_H__
