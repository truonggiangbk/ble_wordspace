#include <string.h>

#include "app_error.h"
#include "boards.h"
#include "nrf_cli.h"
#include "fds.h"
#include "nrf_soc.h"
#include "sdk_config.h"
#include "record.h"
#include "nrf_log.h"

#define RECORD_WAIT_TICKS           8     //1 tick = 125 ms, 24 ticks = 3 sec

/* Array to map FDS return values to strings. */
char const * fds_err_str[] =
{
    "FDS_SUCCESS",
    "FDS_ERR_OPERATION_TIMEOUT",
    "FDS_ERR_NOT_INITIALIZED",
    "FDS_ERR_UNALIGNED_ADDR",
    "FDS_ERR_INVALID_ARG",
    "FDS_ERR_NULL_ARG",
    "FDS_ERR_NO_OPEN_RECORDS",
    "FDS_ERR_NO_SPACE_IN_FLASH",
    "FDS_ERR_NO_SPACE_IN_QUEUES",
    "FDS_ERR_RECORD_TOO_LARGE",
    "FDS_ERR_NOT_FOUND",
    "FDS_ERR_NO_PAGES",
    "FDS_ERR_USER_LIMIT_REACHED",
    "FDS_ERR_CRC_CHECK_FAILED",
    "FDS_ERR_BUSY",
    "FDS_ERR_INTERNAL",
};

/* Flag to check fds initialization. */
static volatile bool m_fds_initialized;

/**@brief   Sleep until an event is received. */
static void power_manage(void)
{
#ifdef SOFTDEVICE_PRESENT
    (void) sd_app_evt_wait();
#else
    __WFE();
#endif
}


/**@brief   Wait for fds to initialize. */
static void wait_for_fds_ready(void)
{
    while (!m_fds_initialized)
    {
        power_manage();
    }
}

static void fds_evt_handler(fds_evt_t const * p_evt)
{
    switch (p_evt->id)
    {
        case FDS_EVT_INIT:
            if (p_evt->result == NRF_SUCCESS)
            {
                m_fds_initialized = true;
            }
            break;
            
        default:
            break;
    }
}

void record_gc(void)
{
    ret_code_t rc = fds_gc();
    switch (rc)
    {
        case NRF_SUCCESS:
            break;

        default:
            NRF_LOG_INFO("error: garbage collection returned %s", fds_err_str[rc]);
            break;
    }
}

ret_code_t record_find(uint32_t fid, uint32_t key)
{
    fds_record_desc_t desc = {0};
    fds_find_token_t  ftok = {0};
    return fds_record_find(fid, key, &desc, &ftok);
}

ret_code_t record_write(uint32_t fid, uint32_t key, void const * p_data, uint32_t len)
{
    fds_record_t const rec =
    {
        .file_id           = fid,
        .key               = key,
        .data.p_data       = p_data,
        .data.length_words = BYTES_TO_WORDS(len)
    };
    
    NRF_LOG_INFO("writing record to flash..."
                    "file: 0x%x, key: 0x%x, \"%s\", len: %u bytes",
                    fid, key, p_data, len);

    ret_code_t rc = fds_record_write(NULL, &rec);
    if (rc != NRF_SUCCESS)
    {
        NRF_LOG_INFO(   "error: fds_record_write() returned %s.",
                        fds_err_str[rc]);
    }
    return rc;
}

ret_code_t record_update(uint32_t fid, uint32_t key, void const * p_data, uint32_t len)
{
    fds_stat_t stat = {0};

    fds_stat(&stat);	
	
    fds_record_desc_t desc = {0};
    fds_find_token_t  ftok = {0};
  
    if (fds_record_find(fid, key, &desc, &ftok) == NRF_SUCCESS)
    {
        fds_record_t const rec =
        {
            .file_id           = fid,
            .key               = key,
            .data.p_data       = p_data,
            .data.length_words = BYTES_TO_WORDS(len)
        };
        ret_code_t rc = fds_record_update(&desc, &rec);
        if (rc != NRF_SUCCESS)
        {
            NRF_LOG_INFO("error: fds_record_update() returned %s.",
                            fds_err_str[rc]);
        }
        return rc;
    }
    else
    {
        NRF_LOG_INFO("error: could not find config file.");
        return FDS_ERR_NOT_FOUND;
    }
}


ret_code_t record_delete(uint32_t fid, uint32_t key)
{
    fds_find_token_t tok   = {0};
    fds_record_desc_t desc = {0};

    NRF_LOG_INFO("deleting record..."
                    "file: 0x%x, key: 0x%x",
                    fid,
                    key);

    if (fds_record_find(fid, key, &desc, &tok) == NRF_SUCCESS)
    {
        ret_code_t rc = fds_record_delete(&desc);
        if (rc != NRF_SUCCESS)
        {
            NRF_LOG_INFO("error: fds_record_delete() returned %s.", fds_err_str[rc]);
        }

        NRF_LOG_INFO("record id: 0x%x", desc.record_id);
        return rc;
    }
    else
    {
        NRF_LOG_INFO("error: record not found!");
        return FDS_ERR_NOT_FOUND;
    }
}

bool record_delete_next(void)
{
    fds_find_token_t  tok   = {0};
    fds_record_desc_t desc  = {0};

    if (fds_record_iterate(&desc, &tok) == NRF_SUCCESS)
    {
        ret_code_t rc = fds_record_delete(&desc);
        if (rc != NRF_SUCCESS)
        {
            return false;
        }

        return true;
    }
    else
    {
        /* No records left to delete. */
        return false;
    }
}

ret_code_t record_read(uint32_t fid, uint32_t key, void const * p_data, uint32_t* len)
{
    fds_record_desc_t desc = {0};
    fds_find_token_t  tok  = {0};

    *len = 0;

    if (fds_record_find(fid, key, &desc, &tok) == NRF_SUCCESS)
    {
        ret_code_t rc;
        fds_flash_record_t frec = {0};

        rc = fds_record_open(&desc, &frec);
        switch (rc)
        {
            case NRF_SUCCESS:
                break;

            case FDS_ERR_CRC_CHECK_FAILED:
                NRF_LOG_INFO("error: CRC check failed!");
                return FDS_ERR_INVALID_ARG;

            case FDS_ERR_NOT_FOUND:
                NRF_LOG_INFO("error: record not found!");
                return FDS_ERR_NOT_FOUND;

            default:
            {
                NRF_LOG_INFO("error: unexpecte error %s.", fds_err_str[rc]);
                return rc;
            }
        }
        *len = frec.p_header->length_words * BYTES_PER_WORD;
        memcpy((uint8_t*)p_data, (uint8_t*)frec.p_data, *len);

        fds_record_close(&desc);
        return NRF_SUCCESS;
    }
    return FDS_ERR_NOT_FOUND;
}

void record_print_all(void)
{
    fds_find_token_t tok   = {0};
    fds_record_desc_t desc = {0};

    NRF_LOG_INFO("rec. id\t"
                    "\tfile id\t"
                    "\trec. key"
                    "\tlength");

    while (fds_record_iterate(&desc, &tok) != FDS_ERR_NOT_FOUND)
    {
        ret_code_t rc;
        fds_flash_record_t config = {0};

        rc = fds_record_open(&desc, &config);
        switch (rc)
        {
            case NRF_SUCCESS:
                break;

            case FDS_ERR_CRC_CHECK_FAILED:
                NRF_LOG_INFO("error: CRC check failed!");
                continue;

            case FDS_ERR_NOT_FOUND:
                NRF_LOG_INFO("error: record not found!");
                continue;

            default:
            {
                NRF_LOG_INFO("error: unexpecte error %s.", fds_err_str[rc]);

                continue;
            }
        }

        uint32_t len = config.p_header->length_words * sizeof(uint32_t);

				NRF_LOG_INFO("----------------\n");
				NRF_LOG_INFO("key: %x\n", desc.record_id);
				NRF_LOG_INFO("data len: %d\n", config.p_header->length_words * 4);
				const uint8_t *pt = config.p_data;
				for(uint16_t i=0;i<len;i++){
					NRF_LOG_INFO("0x%02x ", pt[i]);
				}
				NRF_LOG_INFO("\n");

        fds_record_close(&desc);
    }
}

void record_init(void)
{
    ret_code_t rc;
    
    /* Register first to receive an event when initialization is complete. */
    rc = fds_register(fds_evt_handler);
    APP_ERROR_CHECK(rc);
    
    //NRF_LOG_INFO("Initializing fds...");

    rc = fds_init();
    if(rc != NRF_SUCCESS)
		{
			NVIC_SystemReset();	// reset chip
		}	
    /* Wait for fds to initialize. */
    wait_for_fds_ready();
		
    fds_stat_t stat = {0};

    rc = fds_stat(&stat);
    APP_ERROR_CHECK(rc);		
		
}
